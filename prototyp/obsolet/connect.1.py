import tensorflow as tf
from sklearn import datasets
from sklearn import metrics
from sklearn import model_selection
from tensorflow.contrib.learn import monitors

tensors_to_log = {"loss": "dnn/multi_class_head/softmax_cross_entropy_loss/loss",
                  "acc" : "dnn/multi_class_head/metrics/accuracy/value"}

class _LossCheckerHook(tf.train.LoggingTensorHook):
    def after_run(self,
                  run_context,
                  run_values):
      print(type(run_values.results['loss']))

iris = datasets.load_iris()
x_train, x_test, y_train, y_test = model_selection.train_test_split(
    iris.data,
    iris.target,
    test_size=0.2,
    random_state=42)

feature_columns = tf.contrib.learn.infer_real_valued_columns_from_input(x_train)
classifier = tf.contrib.learn.DNNClassifier(
    feature_columns=feature_columns,
    hidden_units=[10, 20, 10],
    n_classes=3)
    #,
    #model_dir=".",
    #config=tf.contrib.learn.RunConfig(save_checkpoints_secs=1))

hook = _LossCheckerHook(tensors=tensors_to_log, every_n_iter=1)

# Fit and predict.
classifier.fit(x_train, y_train, steps=200, monitors=[hook])
predictions = list(classifier.predict(x_test, as_iterable=True))
score = metrics.accuracy_score(y_test, predictions)
print('Accuracy: {0:f}'.format(score))