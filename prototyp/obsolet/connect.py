import random
import datetime
from pymongo import MongoClient
from pyspark import SparkContext, SparkConf


DB_HOST = "192.168.56.1"
DB_PORT = 27017
DB_NAME = "hop"




CLIENT = MongoClient(DB_HOST, DB_PORT)
DB = CLIENT[DB_NAME]

def createNewExperiment(name, description):
    return {"name": name,
            "startTime": datetime.datetime.utcnow(),
            "endTime": "",
            "description": description}


def createNewJob(experiment_id, parameterJson):
    return {"experimentId": experiment_id,
            "startTime": datetime.datetime.utcnow(),
            "endTime": "None",
            "params" : parameterJson}


def createNewJobLog(job_id, loss, accuracy):
    return {"jobId": job_id,
            "time": datetime.datetime.utcnow(),
            "loss" : loss,
            "accuracy" : accuracy}

def createNewJobResult(job_id, loss, accuracy):
    return {"jobId": job_id,
            "loss" : loss,
            "accuracy" : accuracy}

conf = SparkConf().setAppName("test").setMaster("spark://192.168.56.101:7077")
sc = SparkContext(conf=conf)

num_samples = 10

def inside(p):
    CLIENT = MongoClient(DB_HOST, DB_PORT)
    DB = CLIENT[DB_NAME]
    jobs = DB.experiments.jobs
    job_id = jobs.insert_one(createNewJob(experiment_id, {"test":"test"} )).inserted_id

    for i in range(1, 10):
        jobLogs = DB.experiments.jobs.logs
        jobLogs.insert_one(createNewJobLog(job_id, 10 / i, 10 / i))

    jobResults = DB.experiments.jobs.results
    jobResults.insert_one(createNewJobResult(job_id, 2, 2))

    jobs.update_one({"_id" : job_id}, {'$set' : {"endTime": datetime.datetime.utcnow()}})
    job = jobs.find_one({"_id" : job_id})

    x = random.random() * 2 -1
    y = random.random() * 2 -1
    return x*x + y*y <= 1
    
experiments = DB.experiments
experiment_id = experiments.insert(createNewExperiment("test", ""))

count = sc.parallelize(range(0, num_samples)).filter(inside)
count = count.count()

pi = 4.0 * count / num_samples
print(pi)

experiments.update_one({"_id" : experiment_id}, {'$set': {"endTime": datetime.datetime.utcnow()}})


sc.stop()