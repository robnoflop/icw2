from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import random
import numpy as np
import datetime
from pymongo import MongoClient
from pyspark import SparkContext, SparkConf


DB_HOST = "192.168.56.1"
DB_PORT = 27017
DB_NAME = "hop"


CLIENT = MongoClient(DB_HOST, DB_PORT)
DB = CLIENT[DB_NAME]

def createNewExperiment(name, description):
    return {"name": name,
            "startTime": datetime.datetime.utcnow(),
            "endTime": "",
            "description": description}


def createNewJob(experiment_id, parameterJson):
    return {"experimentId": experiment_id,
            "startTime": datetime.datetime.utcnow(),
            "endTime": "None",
            "params" : parameterJson}


def createNewJobLog(job_id, loss, accuracy):
    return {"jobId": job_id,
            "time": datetime.datetime.utcnow(),
            "loss" : loss,
            "accuracy" : accuracy}

def createNewJobResult(job_id, loss, accuracy):
    return {"jobId": job_id,
            "loss" : loss,
            "accuracy" : accuracy}

conf = SparkConf().setAppName("test").setMaster("spark://192.168.56.101:7077")
sc = SparkContext(conf=conf)

num_samples = 4

def inside(p, parameter):
    import tensorflow as tf
    from sklearn import datasets
    from sklearn import metrics
    from sklearn import model_selection
    from tensorflow.contrib.learn import monitors

    tensors_to_log = {"loss": "dnn/multi_class_head/softmax_cross_entropy_loss/loss",
                        "acc" : "dnn/multi_class_head/metrics/accuracy/value"}


    CLIENT = MongoClient(DB_HOST, DB_PORT)
    DB = CLIENT[DB_NAME]
    jobs = DB.experiments.jobs
    job_id = jobs.insert_one(createNewJob(experiment_id, parameter)).inserted_id

    #SessionRunValues(results={'acc': 0.0, 'loss': 0.058673568}, options=, run_metadata=)
    class _LossCheckerHook(tf.train.LoggingTensorHook):
        def after_run(self,
                    run_context,
                    run_values):
            jobLogs = DB.experiments.jobs.logs
            jobLogs.insert_one(createNewJobLog(job_id,
                                               run_values.results['loss'].item(),
                                               run_values.results['acc'].item()))


    iris = datasets.load_iris()
    x_train, x_test, y_train, y_test = model_selection.train_test_split(
        iris.data,
        iris.target,
        test_size=0.2,
        random_state=42)

    feature_columns = tf.contrib.learn.infer_real_valued_columns_from_input(x_train)
    classifier = tf.contrib.learn.DNNClassifier(
        feature_columns=feature_columns,
        hidden_units=[10, 20, 10],
        n_classes=3)


    hook = _LossCheckerHook(tensors=tensors_to_log, every_n_iter=1)
    # Fit and predict.
    classifier.fit(x_train,
                   y_train,
                   steps=parameter['steps'],
                   batch_size=parameter['batchsize'],
                   monitors=[hook])
    predictions = list(classifier.predict(x_test, as_iterable=True))
    score = metrics.accuracy_score(y_test, predictions)
    print('Accuracy: {0:f}'.format(score))
    
    jobResults = DB.experiments.jobs.results
    jobResults.insert_one(createNewJobResult(job_id, 0 ,score))
    
    # for i in range(1, 10):
    #     jobLogs = DB.experiments.jobs.logs
    #     jobLogs.insert_one(createNewJobLog(job_id, 10 / i, 10 / i))

    jobs.update_one({"_id" : job_id}, {'$set' : {"endTime": datetime.datetime.utcnow()}})
    job = jobs.find_one({"_id" : job_id})

    x = random.random() * 2 -1
    y = random.random() * 2 -1
    return x*x + y*y <= 1
    
experiments = DB.experiments
experiment_id = experiments.insert(createNewExperiment("test", ""))

parameter = []
for i in range(num_samples):
    steps = np.random.randint(10, 200)
    batchsize = [100, 200, 300, 400, 500, 600, 700, 800, 900, 1000]
    index = np.random.randint(0, len(batchsize))
    parameter.append({"steps" : steps, "batchsize": batchsize[index]})

sc.parallelize(range(0, num_samples)).foreach(lambda row: inside(row,parameter[row]))

experiments.update_one({"_id" : experiment_id}, {'$set': {"endTime": datetime.datetime.utcnow()}})

sc.stop()