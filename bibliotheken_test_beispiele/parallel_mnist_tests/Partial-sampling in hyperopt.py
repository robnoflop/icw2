from hyperopt import hp, fmin, rand, tpe
from hyperopt.mongoexp import MongoTrials
from hyperopt.pyll import scope
import mnist_deep

space = hp.choice('a', [-1, hp.uniform('b', 0, 1)])



#bisas_initial, weight_initial, filter_size, fc1_size, conv1_depth, conv2_depth


@scope.define
def conv2Switch(a):
    depths = [4, 8, 16, 32, 64]
    print(depths[2:len(depths) - 1])
    if a == 4:
        return hp.choice('conv2_depth', depths)
    
    if a == 8:
        return hp.choice('conv2_depth', depths[0:len(depths) - 1])

    if a == 16:
        return hp.choice('conv2_depth', depths[1:len(depths) - 1])

    if a == 32:
        return hp.choice('conv2_depth', depths[2:len(depths) - 1])


conv1_depth = hp.choice('conv1_depth', [4, 8, 16, 32])
conv2_depth = hp.choice('conv2_depth', [4, 8, 16, 32, 64])

space = {
    'bisas_initial': hp.uniform('bisas_initial', 0.5, 0.01),
    'weight_initial': hp.uniform('weight_initial', 0.5, 0.01),
    'filter_size': hp.choice('filter_size', [3, 5, 7]),
    'fc1_size': hp.choice('fc1_size', [128, 256, 512, 1024]),
    'conv1_depth': conv1_depth,
    'conv2_depth': conv2_depth
}


#    space = hp.choice('args',
# [
#     ('bisas_initial', [3, 5, 7]),
#     ('weight_initial', [3, 5, 7]),
#     ('filter_size', [3, 5, 7]),
#     ('fc1_size', [128, 256, 512, 1024]),
#     ('conv1_depth', [4, 8, 16, 32]),
#     ('conv2_depth', [8, 16, 32, 64])
# ])

print(space)

trials = MongoTrials('mongo://localhost:1234/foo_db/jobs', exp_key='exp1')
best = fmin(fn=mnist_deep.hyperOptTest, space=space, algo=tpe.suggest, max_evals=10, trials=trials)
print("")
print("##result##")
print(best)


